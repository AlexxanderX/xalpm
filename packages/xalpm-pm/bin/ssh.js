"use strict";

require("core-js/modules/es6.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.promise");

require("core-js/modules/es7.array.includes");

require("core-js/modules/es6.string.includes");

require("regenerator-runtime/runtime");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _default =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(argv) {
    var node_ssh, ssh_url, parsed, ssh;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!argv.ssh) {
              _context.next = 19;
              break;
            }

            node_ssh = require("node-ssh");
            ssh_url = require("ssh-url");
            console.log(node_ssh);
            console.log(ssh_url);
            parsed = ssh_url.parse(argv.ssh);
            console.log(parsed);
            ssh = new node_ssh();
            _context.prev = 8;
            console.log("CONNECTING");
            _context.next = 12;
            return ssh.connect({
              host: parsed.hostname,
              username: parsed.user,
              port: 22,
              tryKeyboard: true,
              onKeyboardInteractive: function onKeyboardInteractive(name, instructions, instructionsLang, prompts, finish) {
                console.log("KEYBOARD");

                if (prompts.length > 0 && prompts[0].prompt.toLowerCase().includes('password')) {
                  finish([password]);
                }
              }
            });

          case 12:
            _context.next = 18;
            break;

          case 14:
            _context.prev = 14;
            _context.t0 = _context["catch"](8);
            console.debug(_context.t0);
            process.exit(1);

          case 18:
            process.exit(0);

          case 19:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[8, 14]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.default = _default;