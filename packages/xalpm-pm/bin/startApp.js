#!/usr/bin/env node
"use strict";

require("core-js/modules/es6.date.now");

require("core-js/modules/es6.function.name");

var _cluster = _interopRequireDefault(require("cluster"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var argv = require("yargs").option("instances", {
  alias: "i"
}).option("file", {
  alias: "f"
}).option("name", {
  alias: "n"
}).option("verbose", {
  alias: "v"
}).default({
  instances: 1
}).demandOption(["file", "name"]).argv;

process.on('unhandledRejection', function (up) {
  throw up;
});

if (_cluster.default.isMaster) {
  var ipc = require("node-ipc");

  var path = require("path");

  console.log("Master ".concat(process.pid, " is running"));

  var config = require("./readConfig").default();

  ipc.config.id = "xalpm_" + argv.name;
  var lastCrashTime = -1;
  var isClosing = false;
  ipc.serve(function () {
    console.log("IPC server started on \"".concat(ipc.config.id, "\""));
    ipc.server.on("error", function (data) {
      console.error(error);
      process.exit(1);
    });
    ipc.server.on("message", function (_data, socket) {
      var data = JSON.parse(_data);
      var response = {
        header: data.header
      };
      console.log(response);

      switch (data.header) {
        case "status":
          {
            if (lastCrashTime != -1 && Date.now() - lastCrashTime < 2000) {
              response.errors = true;
            }

            response.memory = process.memoryUsage().heapUsed / 1024 / 1024;
            break;
          }

        case "kill":
          {
            isClosing = true;

            for (var id in _cluster.default.workers) {
              _cluster.default.workers[id].kill();
            }

            process.exit(0);
            break;
          }
      }

      ipc.server.emit(socket, "message", JSON.stringify(response));
    });
  }); // Fork workers.

  for (var i = 0; i < argv.instances; i++) {
    _cluster.default.fork();
  }

  _cluster.default.on('exit', function (worker, code, signal) {
    console.log("worker ".concat(worker.process.pid, " died"));
    lastCrashTime = Date.now();
    if (!isClosing) _cluster.default.fork();
  });

  ipc.server.start();
} else {
  console.log("Starting ".concat(argv.name, "..."));
  console.log("CLIENT", argv.file);

  require(argv.file);
}