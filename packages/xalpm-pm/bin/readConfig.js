"use strict";

require("core-js/modules/es6.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DefaultFolder = exports.default = exports.ConfigPath = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _jsYaml = _interopRequireDefault(require("js-yaml"));

var _os = _interopRequireDefault(require("os"));

var _path = _interopRequireDefault(require("path"));

var _BaseFolder = _interopRequireDefault(require("./data/BaseFolder"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DEFAULT_CONFIG_PATH = _path.default.join(_BaseFolder.default, "config.yml");

var CONFIG_PATH = process.env.XALPM_PATH || DEFAULT_CONFIG_PATH;
var ConfigPath = CONFIG_PATH;
exports.ConfigPath = ConfigPath;

var _default = function _default() {
  var config = "";

  if (_fs.default.existsSync(CONFIG_PATH)) {
    try {
      config = _jsYaml.default.safeLoad(_fs.default.readFileSync(CONFIG_PATH, "utf8"));
    } catch (error) {
      console.error(error);
    }
  } else {
    config = {
      apps: [],
      socketBasePath: _os.default.tmpdir(),
      socketExtension: "service"
    };

    _fs.default.writeFileSync(CONFIG_PATH, _jsYaml.default.safeDump(config), {
      encoding: "utf8",
      mode: 436
    });
  }

  return config;
};

exports.default = _default;
var DefaultFolder = _BaseFolder.default;
exports.DefaultFolder = DefaultFolder;