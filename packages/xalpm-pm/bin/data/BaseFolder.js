"use strict";

require("core-js/modules/es6.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _os = _interopRequireDefault(require("os"));

var _path = _interopRequireDefault(require("path"));

var _fs = require("fs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BaseFolder = process.env.XALPM_PATH || _path.default.join(_os.default.homedir(), ".xalpm");

if (!(0, _fs.existsSync)(BaseFolder)) {
  var mkdirSync = require("fs").mkdirSync;

  mkdirSync(BaseFolder);
  mkdirSync(_path.default.join(BaseFolder, "logs"));
  mkdirSync(_path.default.join(BaseFolder, "apps"));
}

var _default = BaseFolder;
exports.default = _default;