"use strict";

require("core-js/modules/es6.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = process.env.XALPM_DEBUGGING ? "./node_modules/.bin/babel-node src/index.js" : "xalpm";

exports.default = _default;