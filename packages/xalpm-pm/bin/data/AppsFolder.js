"use strict";

require("core-js/modules/es6.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _os = _interopRequireDefault(require("os"));

var _path = _interopRequireDefault(require("path"));

var _fs = require("fs");

var _BaseFolder = _interopRequireDefault(require("./BaseFolder"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AppPath = _path.default.join(_BaseFolder.default, "apps");

if (!(0, _fs.existsSync)(_BaseFolder.default)) {
  require("fs").mkdirSync(_BaseFolder.default);
}

var _default = AppPath;
exports.default = _default;