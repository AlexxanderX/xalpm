"use strict";

require("core-js/modules/es6.string.includes");

require("core-js/modules/es6.object.keys");

require("core-js/modules/es7.array.includes");

require("core-js/modules/es6.array.for-each");

require("core-js/modules/es6.promise");

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.string.iterator");

require("core-js/modules/es6.array.map");

require("core-js/modules/es6.function.name");

require("regenerator-runtime/runtime");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

exports.command = "start [name..]";
exports.desc = "start apps";

exports.builder = function (yargs) {
  return yargs.option("force", {
    alias: "f",
    describe: "Kill other processes"
  });
};

exports.handler =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(argv) {
    var requireConfigData, config, spawn, path, ipc, ALL, alreadyRunning, apps;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            requireConfigData = require("../readConfig");
            config = requireConfigData.default();
            spawn = require("child_process").spawn;
            path = require("path");
            ipc = require("node-ipc");
            ALL = "all";
            ipc.config.silent = process.env.XALPM_DEBUGGING ? false : true;
            ipc.config.id = "master";
            alreadyRunning = [];
            apps = argv.name.length == 0 && argv.name[0] == ALL ? config.apps.map(function (app) {
              return app.name;
            }) : argv.name; // Find all the apps that are already running

            _context.next = 12;
            return Promise.all(apps.map(function (app) {
              var ipcName = "xalpm_" + app;
              return new Promise(function (resolve) {
                ipc.connectTo(ipcName, function () {
                  ipc.of[ipcName].on("connect", function () {
                    alreadyRunning.push(app);
                    ipc.disconnect(ipcName);
                    resolve();
                  });
                  ipc.of[ipcName].on("error", function () {
                    ipc.disconnect(ipcName);
                    resolve();
                  });
                });
              });
            }));

          case 12:
            alreadyRunning.forEach(function (app) {
              return console.log("App \"".concat(app, "\" is already running..."));
            });
            apps.forEach(function (appName) {
              var app;

              for (var i = 0, length = config.apps.length; i < length; ++i) {
                if (config.apps[i].name == appName) {
                  app = config.apps[i];
                  break;
                }
              }

              console.log("APP", app);
              if (alreadyRunning.includes(app.name)) return;
              var isLogging = app.no_logging ? "" : " >>".concat(path.join(requireConfigData.DefaultFolder, "logs", app.name + ".log"), " 2>&1");
              var startApp = process.env.XALPM_DEBUGGING ? "node_modules/.bin/babel-node src/startApp.js" : "xalpm-startapp";
              var env = "";

              if (app.env) {
                Object.keys(app.env).forEach(function (envName) {
                  return env += "".concat(envName, "=").concat(app.env[envName], " ");
                });
              }

              console.log(env);
              var command = "".concat(env).concat(startApp, " -n ").concat(app.name, " -f ").concat(app.script).concat(isLogging);
              console.debug("Executing ".concat(command));
              console.log("Starting ".concat(app.name, "..."));
              spawn(command, [], {
                detached: true,
                shell: true
              });
            });
            _context.next = 16;
            return require("../utils/sleep").default(2000);

          case 16:
            _context.next = 18;
            return require("./list").handler();

          case 18:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();