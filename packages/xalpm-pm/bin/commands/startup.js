"use strict";

require("core-js/modules/es6.promise");

require("regenerator-runtime/runtime");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

exports.command = "startup";
exports.desc = "make the app to start at os startup";

exports.builder = function (yargs) {
  return yargs.option("s", {
    alias: "service",
    desc: "The service app to use",
    choices: ["systemctl"],
    demandOption: true
  }).option("u", {
    alias: "user",
    desc: "the user for which the script to start for in order to use as root"
  });
};

exports.handler =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(argv) {
    var fs, path, os, execSync, XALPM, systemdPath, access, username, script, serviceName, serviceFilePath;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            access = function _ref2(path, mode) {
              try {
                fs.accessSync(path, mode);
                return true;
              } catch (error) {
                return false;
              }
            };

            fs = require("fs");
            path = require("path");
            os = require("os");
            execSync = require("child_process").execSync;
            XALPM = require("../data/XALPM").default;
            systemdPath = path.join("/etc", "systemd", "system");
            _context.t0 = argv.service;
            _context.next = _context.t0 === "systemctl" ? 10 : 21;
            break;

          case 10:
            username = argv.user || os.userInfo().username;

            if (!access(systemdPath, fs.constants.W_OK)) {
              console.log("Run this command as root/sudo:\n".concat(XALPM, " startup --service ").concat(argv.service, " --user ").concat(username));
              process.exit(0);
            }

            script = "\n            [Unit]\n            Description=XALPM\n            After=network-online.target\n            \n            [Service]\n            Restart=always\n            RestartSec=3\n            User=".concat(username, "\n            \n            ExecStart=/usr/lib/node_modules/xalpm/bin/xalpm start all\n            ExecReload=/usr/lib/node_modules/xalpm/bin/xalpm reload all\n            ExecStop=/usr/lib/node_modules/xalpm/bin/xalpm kill all\n            \n            [Install]\n            WantedBy=multi-user.target\n            ");
            serviceName = "xalpm-".concat(username);
            serviceFilePath = path.join(systemdPath, "".concat(serviceName, ".service"));
            console.log("Creating the service ".concat(serviceName, " (").concat(serviceFilePath, ")"));
            fs.writeFileSync(serviceFilePath, script);
            console.log("Starting the service...");
            execSync("systemctl enable ".concat(serviceName));
            console.log("Done! :D");
            return _context.abrupt("break", 21);

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();