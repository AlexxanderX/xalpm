"use strict";

require("core-js/modules/es6.promise");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.string.iterator");

require("core-js/modules/es7.array.includes");

require("core-js/modules/es6.string.includes");

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.for-each");

require("core-js/modules/es6.function.name");

require("core-js/modules/es6.array.map");

require("regenerator-runtime/runtime");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

exports.command = "kill [name..]";
exports.desc = "kill app";

exports.builder = function (yargs) {
  return yargs.positional("name", {
    describe: "the app to kill"
  });
};

exports.handler =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(argv) {
    var ipc, config, appsNames, sleep;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            sleep = function _ref2(ms) {
              return new Promise(function (resolve) {
                return setTimeout(resolve, ms);
              });
            };

            ipc = require("node-ipc");
            config = require("../readConfig").default();
            appsNames = config.apps.map(function (app) {
              return app.name;
            });
            argv.name.forEach(function (name) {
              if (!appsNames.includes(name)) {
                console.error("The app \"".concat(name, "\" doesn't exists!"));
                process.exit(1);
              }
            });
            ipc.config.silent = process.env.XALPM_DEBUGGING ? false : true;
            ipc.config.id = "master";
            _context.next = 9;
            return Promise.all(argv.name.map(function (app) {
              var ipcName = "xalpm_" + app;
              return new Promise(function (resolve) {
                ipc.connectTo(ipcName, function () {
                  ipc.of[ipcName].on("connect", function () {
                    console.log("SENDING KILL MESSAGE");
                    ipc.of[ipcName].emit("message", JSON.stringify({
                      header: "kill"
                    }));
                    ipc.disconnect(ipcName);
                    resolve();
                  });
                  ipc.of[ipcName].on("error", function () {
                    ipc.disconnect(ipcName);
                    resolve();
                  });
                });
              });
            }));

          case 9:
            _context.next = 11;
            return sleep(2000);

          case 11:
            require("./list").handler();

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();