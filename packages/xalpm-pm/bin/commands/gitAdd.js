"use strict";

require("core-js/modules/es6.promise");

require("core-js/modules/es6.function.name");

require("core-js/modules/es7.array.includes");

require("core-js/modules/es6.string.includes");

require("core-js/modules/es6.array.for-each");

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.object.keys");

require("regenerator-runtime/runtime");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

exports.command = "add [config]";
exports.desc = "add an app from a git repository";

exports.builder = function (yargs) {
  return yargs.positional("config", {
    describe: "the config file for the app"
  }).option("run-npm-install", {
    describe: "run 'npm install' after cloning the repository"
  });
};

exports.handler =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(argv) {
    var yaml, fs, appConfig, requiredFields, allFields, appConfigKeys, configData, config, join, simpleGit, cloneOptions, execSync;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return require("../ssh").default("git add", argv);

          case 2:
            yaml = require("js-yaml");
            fs = require("fs");

            try {
              appConfig = yaml.safeLoad(fs.readFileSync(argv.config, "utf8"));
            } catch (error) {
              console.debug(error);
              console.error("The config file was not found or it has syntax errors");
              process.exit(1);
            }

            requiredFields = ["name", "script", "git_repo"];
            allFields = requiredFields.concat(["no_logging", "log_type", "log_path", "git_branch", "env", "path"]);
            appConfigKeys = Object.keys(appConfig); // Check that config has all the required fields

            requiredFields.forEach(function (field) {
              if (!appConfigKeys.includes(field)) {
                console.error("The field \"".concat(field, "\" is required and was not found in the config."));
                process.exit(1);
              }
            }); // Check that the config doesn't have an unrecognized field

            appConfigKeys.forEach(function (field) {
              if (!allFields.includes(field)) {
                console.error("The field \"".concat(field, "\" is not recognized"));
                process.exit(1);
              }
            });
            configData = require("../readConfig");
            config = configData.default();
            config.apps.forEach(function (app) {
              if (app.name == appConfig.name) {
                console.error("The app name \"".concat(appConfig.name, "\" already exists."));
                process.exit(1);
              }
            });
            join = require("path").join;
            if (!appConfig.path) appConfig.path = join(require("../data/AppsFolder").default, appConfig.name); // Git clone the repository

            if (fs.existsSync(appConfig.path)) {
              console.error("The path ".concat(appConfig.path, " already exists"));
              process.exit(1);
            }

            simpleGit = require("simple-git/promise")();
            cloneOptions = ['--single-branch'];

            if (appConfig.git_branch) {
              cloneOptions.push("-b".concat(appConfig.git_branch));
            }

            console.log(cloneOptions);
            console.log("Cloning the repository to ".concat(appConfig.path));
            _context.prev = 21;
            _context.next = 24;
            return simpleGit.clone(appConfig.git_repo, appConfig.path, cloneOptions);

          case 24:
            _context.next = 30;
            break;

          case 26:
            _context.prev = 26;
            _context.t0 = _context["catch"](21);
            console.error("Git clone error:", _context.t0);
            process.exit(1);

          case 30:
            if (argv.runNpmInstall) {
              console.log("Running 'npm install'");
              execSync = require("child_process").execSync;

              try {
                execSync("npm install", {
                  cwd: appConfig.path
                });
              } catch (error) {
                console.error("Something bad happened while running \"npm install\"");
                console.error(error);
                process.exit(1);
              }
            }

            appConfig.script = join(appConfig.path, appConfig.script);
            if (!appConfig.env) appConfig.env = [];
            config.apps.push(appConfig);
            fs.writeFileSync(configData.ConfigPath, yaml.safeDump(config));
            console.log("Run \"xalpm start ".concat(appConfig.name, "\" to start the app"));
            process.exit(0);

          case 37:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[21, 26]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();