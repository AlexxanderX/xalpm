"use strict";

require("core-js/modules/es6.promise");

require("core-js/modules/es6.function.name");

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.for-each");

require("regenerator-runtime/runtime");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

exports.command = "update [app..]";
exports.desc = "update from repository the app";

exports.builder = function (yargs) {
  return yargs.positional("app", {
    desc: "the app/apps to update"
  });
};

exports.handler =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(argv) {
    var config;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            config = require("../readConfig").default();
            argv.app.forEach(
            /*#__PURE__*/
            function () {
              var _ref2 = _asyncToGenerator(
              /*#__PURE__*/
              regeneratorRuntime.mark(function _callee(appName) {
                var app, i, length;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        app = null;

                        for (i = 0, length = config.apps.length; i < length; ++i) {
                          if (config.apps[i].name == appName) {
                            app = config.apps[i];
                          }
                        }

                        if (app == null) {
                          console.error("The app ".concat(appName, " doesn't exists."));
                          process.exit(1);
                        }

                        console.log("Updating app \"".concat(appName, "\""));
                        _context.prev = 4;
                        _context.next = 7;
                        return require("simple-git/promise")(app.path).pull();

                      case 7:
                        console.log("Successfully updated \"".concat(appName, "\""));
                        _context.next = 14;
                        break;

                      case 10:
                        _context.prev = 10;
                        _context.t0 = _context["catch"](4);
                        console.error("Errors while updating \"".concat(appName, "\""));
                        console.error(_context.t0);

                      case 14:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this, [[4, 10]]);
              }));

              return function (_x2) {
                return _ref2.apply(this, arguments);
              };
            }());

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();