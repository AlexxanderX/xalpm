"use strict";

require("core-js/modules/es6.function.name");

require("core-js/modules/es6.array.map");

require("core-js/modules/es6.promise");

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.string.iterator");

require("regenerator-runtime/runtime");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

exports.command = "list";
exports.desc = "list all processes";

exports.builder = function (yargs) {
  return yargs;
};

exports.handler =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(argv) {
    var table, colors, ipc, config, BULLET_SYMBOL, tableData;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            table = require("table").table;
            colors = require("colors");
            ipc = require("node-ipc");
            config = require("../readConfig").default();
            ipc.config.silent = process.env.XALPM_DEBUGGING ? false : true;
            ipc.config.id = "master";
            BULLET_SYMBOL = "■";
            tableData = [[" ", "App", "Instances", "Memory usage"]];
            _context.next = 10;
            return Promise.all(config.apps.map(function (app, index) {
              tableData.push([BULLET_SYMBOL, app.name, app.instances || 1, 0]);
              var ipcName = "xalpm_" + app.name;
              return new Promise(function (resolve) {
                var timerID = setTimeout(function () {
                  ipc.disconnect(ipcName);
                  resolve();
                }, 5000);
                ipc.connectTo(ipcName, function () {
                  ipc.of[ipcName].on("connect", function () {
                    ipc.of[ipcName].emit("message", JSON.stringify({
                      header: "status"
                    }));
                  });
                  ipc.of[ipcName].on("error", function () {
                    clearTimeout(timerID);
                    ipc.disconnect(ipcName);
                    resolve();
                  });
                  ipc.of[ipcName].on("message", function (_data) {
                    clearTimeout(timerID);
                    var data = JSON.parse(_data);
                    tableData[index + 1][0] = data.errors ? colors.red(BULLET_SYMBOL) : colors.green(BULLET_SYMBOL);
                    tableData[index + 1][3] = "".concat(data.memory.toFixed(2), "Mb");
                    ipc.disconnect(ipcName);
                    resolve();
                  });
                });
              });
            }));

          case 10:
            console.log(table(tableData));
            console.log("".concat(BULLET_SYMBOL, " - not running "));
            console.log("".concat(colors.green(BULLET_SYMBOL), " - running"));
            console.log("".concat(colors.red(BULLET_SYMBOL), " - errors"));

          case 14:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();