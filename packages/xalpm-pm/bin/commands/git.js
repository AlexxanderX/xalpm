"use strict";

var _gitAdd = _interopRequireDefault(require("./gitAdd"));

var _gitUpdate = _interopRequireDefault(require("./gitUpdate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.command = "git";
exports.desc = "commands for git apps";

exports.builder = function (yargs) {
  return yargs.command(_gitAdd.default).command(_gitUpdate.default).demandCommand(1, '');
};