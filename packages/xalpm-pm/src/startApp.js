#!/usr/bin/env node
import cluster from "cluster";

let argv = require("yargs")
    .option("instances", { alias: "i" })
    .option("file", { alias: "f" })
    .option("name", { alias: "n" })
    .option("verbose", { alias: "v" })
    .default({ instances: 1 })
    .demandOption(["file", "name"])
    .argv;

process.on('unhandledRejection', up => { throw up });

if (cluster.isMaster) {
    const ipc = require("node-ipc");
    const path = require("path");

    console.log(`Master ${process.pid} is running`);

    const config = require("readConfig").default();

    ipc.config.id = "xalpm_" + argv.name;

    let lastCrashTime = -1;

    let isClosing = false;

    ipc.serve(() => {
        console.log(`IPC server started on "${ipc.config.id}"`);

        ipc.server.on("error", (data) => {
            console.error(error);
            process.exit(1);
        });

        ipc.server.on("message", (_data, socket) => {
            const data = JSON.parse(_data);
            let response = { header: data.header };

            console.log(response);

            switch (data.header) {
                case "status": {
                    if (lastCrashTime != -1 && Date.now() - lastCrashTime < 2000) {
                        response.errors = true;
                    }
    
                    response.memory = process.memoryUsage().heapUsed / 1024 / 1024;
                    break;
                }
                
                case "kill": {
                    isClosing = true;

                    for (let id in cluster.workers) {
                        cluster.workers[id].kill();
                    }

                    process.exit(0);
                    break;
                }
            }

            ipc.server.emit(socket, "message", JSON.stringify(response));
        });
    });

    // Fork workers.
    for (let i = 0; i < argv.instances; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);

        lastCrashTime = Date.now();

        if (!isClosing) cluster.fork();
    });

    ipc.server.start();
} else {
    console.log(`Starting ${argv.name}...`);
    console.log("CLIENT", argv.file);
    require(argv.file);
}