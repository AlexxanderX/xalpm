import os from "os";
import path from "path";
import { existsSync } from "fs";

import BaseFolder from "./BaseFolder"; 

const AppPath = path.join(BaseFolder, "apps");

if (!existsSync(BaseFolder)) {
    require("fs").mkdirSync(BaseFolder);
}

export default AppPath;