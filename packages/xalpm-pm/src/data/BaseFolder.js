import os from "os";
import path from "path";
import { existsSync } from "fs";

const BaseFolder = process.env.XALPM_PATH || path.join(os.homedir(), ".xalpm");

if (!existsSync(BaseFolder)) {
    const mkdirSync = require("fs").mkdirSync;
    mkdirSync(BaseFolder);
    mkdirSync(path.join(BaseFolder, "logs"));
    mkdirSync(path.join(BaseFolder, "apps"));
}

export default BaseFolder;