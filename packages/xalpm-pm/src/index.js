#!/usr/bin/env node
import commandList from "./commands/list";
import commandStart from "./commands/start";
import commandKill from "./commands/kill";
import commandGit from "./commands/git";
import commandStartup from "./commands/startup";

const NAME = "xalpm";
const VERSION = "0.0.1";

if (!process.env.XALPM_DEBUGGING) console.debug = ()=>{};
else                              console.debug = function () { console.log("[DEBUG]", ...arguments); };

require("yargs")
    .option("ssh", { describe: "the ssh url to the server if is not local" })
    .command(commandStart)
    .command(commandList)
    .command(commandKill)
    .command(commandGit)
    .command(commandStartup)
    .demandCommand(1, '')
    .alias("list", "l")
    .help("h")
    .alias("h", "help")
    .argv;