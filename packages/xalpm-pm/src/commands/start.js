exports.command = "start [name..]";
exports.desc = "start apps";
exports.builder = (yargs) => {
    return yargs
        .option("force", { alias: "f", describe: "Kill other processes" });
};
exports.handler = async (argv) => {
    const requireConfigData = require("../readConfig");
    const config = requireConfigData.default();
    const spawn = require("child_process").spawn;
    const path = require("path");
    const ipc = require("node-ipc");

    const ALL = "all";

    ipc.config.silent = process.env.XALPM_DEBUGGING ? false : true;
    ipc.config.id = "master";

    let alreadyRunning = [];
    const apps = (argv.name.length == 0 && argv.name[0] == ALL) ? config.apps.map(app => app.name) : argv.name;

    // Find all the apps that are already running
    await Promise.all(
        apps.map(app => {
            const ipcName = "xalpm_" + app;
            return new Promise((resolve) => {
                ipc.connectTo(ipcName, () => {
                    ipc.of[ipcName].on("connect", () => {
                        alreadyRunning.push(app);
                        ipc.disconnect(ipcName);
                        resolve();
                    });

                    ipc.of[ipcName].on("error", () => {
                        ipc.disconnect(ipcName);
                        resolve();
                    })
                });
            });
        })
    );

    alreadyRunning.forEach(app => console.log(`App "${app}" is already running...`));

    apps.forEach(appName => {
        let app;
        for (let i=0, length = config.apps.length; i < length; ++i) {
            if (config.apps[i].name == appName) {
                app = config.apps[i];
                break;
            }
        }

        console.log("APP", app);

        if (alreadyRunning.includes(app.name)) return;

        const isLogging = app.no_logging ? "" : ` >>${path.join(requireConfigData.DefaultFolder, "logs", app.name + ".log")} 2>&1`;

        const startApp = process.env.XALPM_DEBUGGING ? "node_modules/.bin/babel-node src/startApp.js" : "xalpm-startapp";

        let env = "";
        if (app.env) {
            Object.keys(app.env).forEach(envName => env += `${envName}=${app.env[envName]} `);
        }

        console.log(env);

        const command = `${env}${startApp} -n ${app.name} -f ${app.script}${isLogging}`;

        console.debug(`Executing ${command}`);

        console.log(`Starting ${app.name}...`);
        spawn(
            command,
            [],
            {
                detached: true,
                shell: true
            }
        );
    });

    await require("utils/sleep").default(2000);

    await require("./list").handler();
}