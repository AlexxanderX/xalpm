exports.command = "update [app..]";
exports.desc = "update from repository the app";
exports.builder = (yargs) => {
    return yargs
        .positional("app", { desc: "the app/apps to update" });
};
exports.handler = async (argv) => {
    let config = require("../readConfig").default();

    argv.app.forEach(async appName => {
        let app = null;
        for (let i=0, length = config.apps.length; i < length; ++i) {
            if (config.apps[i].name == appName) {
                app = config.apps[i];
            }
        }

        if (app == null) {
            console.error(`The app ${appName} doesn't exists.`);
            process.exit(1);
        }

        console.log(`Updating app "${appName}"`);
        try {
            await require("simple-git/promise")(app.path).pull();
            console.log(`Successfully updated "${appName}"`)
        } catch (error) {
            console.error(`Errors while updating "${appName}"`);
            console.error(error);
        }
        
    });
}