exports.command = "startup";
exports.desc = "make the app to start at os startup";
exports.builder = (yargs) => {
    return yargs
        .option("s", { alias: "service", desc: "The service app to use", choices: ["systemctl"], demandOption: true })
        .option("u", { alias: "user", desc: "the user for which the script to start for in order to use as root" });
};
exports.handler = async (argv) => {
    const fs = require("fs");
    const path = require("path");
    const os = require("os");
    const execSync = require("child_process").execSync;
    const XALPM = require("data/XALPM").default;

    const systemdPath = path.join("/etc", "systemd", "system");

    function access(path, mode) {
        try {
            fs.accessSync(path, mode);
            return true;
        } catch (error) {
            return false;
        }
    }

    switch (argv.service) {
        case "systemctl": {
            const username = argv.user || os.userInfo().username;

            if (!access(systemdPath, fs.constants.W_OK)) {
                console.log(`Run this command as root/sudo:\n${XALPM} startup --service ${argv.service} --user ${username}`);
                process.exit(0);
            }

            const script = `
            [Unit]
            Description=XALPM
            After=network-online.target
            
            [Service]
            Restart=always
            RestartSec=3
            User=${username}
            
            ExecStart=/usr/lib/node_modules/xalpm/bin/xalpm start all
            ExecReload=/usr/lib/node_modules/xalpm/bin/xalpm reload all
            ExecStop=/usr/lib/node_modules/xalpm/bin/xalpm kill all
            
            [Install]
            WantedBy=multi-user.target
            `;

            const serviceName = `xalpm-${username}`;
            const serviceFilePath = path.join(systemdPath, `${serviceName}.service`);

            console.log(`Creating the service ${serviceName} (${serviceFilePath})`);
            fs.writeFileSync(serviceFilePath, script);

            console.log("Starting the service...");
            execSync(`systemctl enable ${serviceName}`);

            console.log("Done! :D");
            break;
        }
    }
}