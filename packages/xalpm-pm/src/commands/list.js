exports.command = "list";
exports.desc = "list all processes";
exports.builder = (yargs) => {
    return yargs;
};
exports.handler = async (argv) => {
    const table = require("table").table;
    const colors = require("colors");
    const ipc = require("node-ipc");
    let config = require("../readConfig").default();

    ipc.config.silent = process.env.XALPM_DEBUGGING ? false : true;
    ipc.config.id = "master";

    const BULLET_SYMBOL = "■";

    let tableData = [[" ", "App", "Instances", "Memory usage"]];

    await Promise.all(
        config.apps.map((app, index) => {
            tableData.push([BULLET_SYMBOL, app.name, app.instances || 1, 0]);

            const ipcName = "xalpm_" + app.name;
            return new Promise((resolve) => {
                let timerID = setTimeout(() => {
                    ipc.disconnect(ipcName);
                    resolve();
                }, 5000);

                ipc.connectTo(ipcName, () => {
                    ipc.of[ipcName].on("connect", () => {
                        ipc.of[ipcName].emit("message", JSON.stringify({ header: "status" }));
                    });

                    ipc.of[ipcName].on("error", () => {
                        clearTimeout(timerID);
                        ipc.disconnect(ipcName);
                        resolve();
                    })

                    ipc.of[ipcName].on("message", (_data) => {
                        clearTimeout(timerID);

                        const data = JSON.parse(_data);

                        tableData[index + 1][0] = data.errors ? colors.red(BULLET_SYMBOL) : colors.green(BULLET_SYMBOL);
                        tableData[index + 1][3] = `${data.memory.toFixed(2)}Mb`;
                        ipc.disconnect(ipcName);
                        resolve();
                    });
                });
            });
        })
    );

    console.log(table(tableData));
    console.log(`${BULLET_SYMBOL} - not running `);
    console.log(`${colors.green(BULLET_SYMBOL)} - running`);
    console.log(`${colors.red(BULLET_SYMBOL)} - errors`);
}