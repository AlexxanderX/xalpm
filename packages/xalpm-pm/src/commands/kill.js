exports.command = "kill [name..]";
exports.desc = "kill app";
exports.builder = (yargs) => {
    return yargs
        .positional("name", { describe: "the app to kill" });
};
exports.handler = async (argv) => {
    const ipc = require("node-ipc");
    let config = require("../readConfig").default();

    const appsNames = config.apps.map(app => app.name);

    argv.name.forEach(name => {
        if (!appsNames.includes(name)) {
            console.error(`The app "${name}" doesn't exists!`);
            process.exit(1);
        }
    })

    ipc.config.silent = process.env.XALPM_DEBUGGING ? false : true;
    ipc.config.id = "master";

    await Promise.all(
        argv.name.map(app => {
            const ipcName = "xalpm_" + app;
            return new Promise((resolve) => {
                ipc.connectTo(ipcName, () => {
                    ipc.of[ipcName].on("connect", () => {
                        console.log("SENDING KILL MESSAGE");
                        ipc.of[ipcName].emit("message", JSON.stringify({ header: "kill" }));
                        ipc.disconnect(ipcName);
                        resolve();
                    });

                    ipc.of[ipcName].on("error", () => {
                        ipc.disconnect(ipcName);
                        resolve();
                    })
                });
            });
        })
    );

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    await sleep(2000);

    require("./list").handler();
}