import commandAdd from "./gitAdd";
import commandUpdate from "./gitUpdate";

exports.command = "git";
exports.desc = "commands for git apps";
exports.builder = (yargs) => {
    return yargs
        .command(commandAdd)
        .command(commandUpdate)
        .demandCommand(1, '');
};