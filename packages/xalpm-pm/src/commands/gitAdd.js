exports.command = "add [config]";
exports.desc = "add an app from a git repository";
exports.builder = (yargs) => {
    return yargs
        .positional("config", { describe: "the config file for the app" })
        .option("run-npm-install", { describe: "run 'npm install' after cloning the repository" })
};
exports.handler = async (argv) => {
    await require("ssh").default("git add", argv);

    const yaml = require("js-yaml");
    const fs = require("fs");

    let appConfig;
    try {
        appConfig = yaml.safeLoad(fs.readFileSync(argv.config, "utf8")); 
    } catch (error) {
        console.debug(error);
        console.error("The config file was not found or it has syntax errors");
        process.exit(1);
    }
    
    const requiredFields = [ 
        "name", 
        "script",
        "git_repo"
    ];

    const allFields = [ 
        ...requiredFields, 
        "no_logging",
        "log_type", 
        "log_path",
        "git_branch",
        "env",
        "path"
    ];

    let appConfigKeys = Object.keys(appConfig);
    
    // Check that config has all the required fields
    requiredFields.forEach(field => {
        if (!appConfigKeys.includes(field)) {
            console.error(`The field "${field}" is required and was not found in the config.`);
            process.exit(1);
        }
    });

    // Check that the config doesn't have an unrecognized field
    appConfigKeys.forEach(field => {
        if (!allFields.includes(field)) {
            console.error(`The field "${field}" is not recognized`);
            process.exit(1);
        }
    });

    let configData = require("../readConfig");
    let config = configData.default();

    config.apps.forEach(app => {
        if (app.name == appConfig.name) {
            console.error(`The app name "${appConfig.name}" already exists.`);
            process.exit(1);
        }
    })
    
    const join = require("path").join;

    if (!appConfig.path) appConfig.path = join(require("data/AppsFolder").default, appConfig.name);

    // Git clone the repository

    if (fs.existsSync(appConfig.path)) {
        console.error(`The path ${appConfig.path} already exists`);
        process.exit(1);
    }

    const simpleGit = require("simple-git/promise")();

    let cloneOptions = [ '--single-branch' ];
    if (appConfig.git_branch) {
        cloneOptions.push(`-b${appConfig.git_branch}`);
    }

    console.log(cloneOptions);
    
    console.log(`Cloning the repository to ${appConfig.path}`);
    try {
        await simpleGit.clone(
            appConfig.git_repo,
            appConfig.path,
            cloneOptions
        );
    } catch (error) {
        console.error("Git clone error:", error);
        process.exit(1);
    }

    if (argv.runNpmInstall) {
        console.log("Running 'npm install'");

        const execSync = require("child_process").execSync;
        try {
            execSync("npm install", {
                cwd: appConfig.path
            });
        } catch (error) {
            console.error("Something bad happened while running \"npm install\"");
            console.error(error);
            process.exit(1);
        }
    }

    appConfig.script = join(appConfig.path, appConfig.script);
    if (!appConfig.env) appConfig.env = [];

    config.apps.push(appConfig);
    fs.writeFileSync(configData.ConfigPath, yaml.safeDump(config));

    console.log(`Run "xalpm start ${appConfig.name}" to start the app`);
    
    process.exit(0);
}