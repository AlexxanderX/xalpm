import fs from "fs";
import yaml from "js-yaml";
import os from "os";
import path from "path";

import DEFAULT_FOLDER from "data/BaseFolder";

const DEFAULT_CONFIG_PATH = path.join(DEFAULT_FOLDER, "config.yml");

const CONFIG_PATH = process.env.XALPM_PATH || DEFAULT_CONFIG_PATH;

export const ConfigPath = CONFIG_PATH;

export default () => {
    let config = "";
    
    if (fs.existsSync(CONFIG_PATH)) {
        try {
            config = yaml.safeLoad(fs.readFileSync(CONFIG_PATH, "utf8"));
        } catch (error) {
            console.error(error);
        }
    } else {
        config = {
            apps: [],
            socketBasePath: os.tmpdir(),
            socketExtension: "service"
        }
    
        fs.writeFileSync(CONFIG_PATH, yaml.safeDump(config), { encoding: "utf8", mode: 0o664 });
    }

    return config;
}

export const DefaultFolder = DEFAULT_FOLDER;