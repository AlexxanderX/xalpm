export default async (argv) => {
    if (argv.ssh) {
        const node_ssh = require("node-ssh");
        const ssh_url = require("ssh-url");

        console.log(node_ssh);
        console.log(ssh_url);

        const parsed = ssh_url.parse(argv.ssh);

        console.log(parsed);

        const ssh = new node_ssh();
        
        try {
            console.log("CONNECTING");
            await ssh.connect({
                host: parsed.hostname,
                username: parsed.user,
                port: 22,
                tryKeyboard: true,
                onKeyboardInteractive: (name, instructions, instructionsLang, prompts, finish) => {
                    console.log("KEYBOARD");
                    if (prompts.length > 0 && prompts[0].prompt.toLowerCase().includes('password')) {
                        finish([password]);
                    }
                }
            });
        } catch (error) {
            console.debug(error);
            process.exit(1);
        }

        process.exit(0);
    }
}